/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// https://championswimmer.in/vuex-module-decorators/pages/overview.html#what-it-does
// https://blog.csdn.net/sllailcp/article/details/106402583//
import { Module, VuexModule, Mutation, Action, getModule } from 'vuex-module-decorators'
import store from '@/store'
type User = {name: string; password: string}
// 动态模块，只有组件内使用的时候才会注册
@Module({name: 'moduleB', namespaced: true, dynamic: true, store: store})
class moduleB extends VuexModule {
  // state
  public loginInfo: User[] = []
  
  // getter
  get userNumber():number {
    return this.loginInfo.length
  }

  // mutation
  @Mutation
  USERINFO(user: User): void {
    console.log(user)
    this.loginInfo.push(user)
  }

  // action
  // commit两种调用方式：1、在Action后边添加commit，最后return的值即为传入USERINFO的参数
  @Action({ commit: 'USERINFO' })
  ADD_USER_ONE(): User {
    return { name: '张三', password: '123'  }
  }
  // 2、在action中通过this.XXX(mutation名称)调用并传值
  @Action
  ADD_USER_TWO(obj: any): void {
    console.log(obj)
    this.context.commit('USERINFO', obj)
    // this.USERINFO(obj)
  }
}
export const moduleBModule = getModule(moduleB)